(ns hike-advise.output.report
  (:require [hike-advise.config :as config]))

(declare format-segment-results generate-spaces)

(defn format-results [segment-list]
  (->> segment-list
       (map #(str (:file %)
                  "\n"
                  (->> % format-segment-results vec (apply str)))))) ; NEED vec is omdat het een lazy seq is, betere oplossing?

(defn format-segment-results [segment]
  (->> segment
       :results
       seq
       (map #(str "\t"
                  (-> % first name)
                  (-> % first generate-spaces)
                  (-> % second)
                  "\n"))))

; also works for words (strings)
(defn generate-spaces [keyword]
  (->> " "
       repeat
       (take (- config/WIDTH_COLUMN (count (name keyword))))
       (apply str)))
