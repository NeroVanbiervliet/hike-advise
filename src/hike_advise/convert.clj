(ns hike-advise.convert)

(defn extract-from-segment [segment data-key]
  (->> (:data segment)
      (map data-key)))

(defn write-to-segment [segment data-key data]
  (->> (map #(assoc % data-key %2) (:data segment) data)
      (assoc segment :data)))
