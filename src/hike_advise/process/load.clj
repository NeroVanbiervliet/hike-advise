(ns hike-advise.process.load
  "Returns the intensitity of a segment. 100% = 1 full day."
  (:require [hike-advise.process.elevation :as elevation]
            [hike-advise.process.distance :as distance]))

(defn calc-hike-load [segment]
  (+ (* 0.0303 (elevation/total-asc segment))
     (* 0.0276 (elevation/total-desc segment))
     (* 3.7428 (distance/total-km segment))))

(defn write-result [segment]
  (assoc-in segment [:results :hike-load] (calc-hike-load segment)))
