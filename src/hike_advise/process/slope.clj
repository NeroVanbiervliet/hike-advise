(ns hike-advise.process.slope
  (:require [hike-advise.convert :refer [write-to-segment extract-from-segment]]
            [hike-advise.config :as config]))

(declare slope median filter-seqs)

(defn calc [segment]
  (->> (:data segment)
       (#(map slope % (rest %)))
       (cons 0)
       (filter-seqs config/MEDIAN_SAMPLES)
       (apply map median))) ; NEED some data loss (nieuwe seq is korter) oplossen door filter-seqs op het einde te padden met laatste value van data

(defn write-calc [segment]
  (->> (calc segment)
       (write-to-segment segment :slope)))

(defn slope [p1 p2]
  (let [{dist1 :distance elev1 :elevation} p1
        {dist2 :distance elev2 :elevation} p2]
       (cond-> (- elev2 elev1)
         (not (== 0 dist2)) (-> (/ dist2) (/ 10)) ; 10 = [m] -> [km] + percentage (%)
         :else (+ 0)))) ; returns 0+0 = 0

(def nearly-same-dist
  (comparator
   (fn [p1 p2]
     (let [d1 (:cumul-distance p1) d2 (:cumul-distance p2)]
       (< (+ d1 config/NEAR_DISTANCE) d2)))))

(defn issue-too-steep-points [segment]
  (->> (:data segment)
       (filter #(-> (:slope %)
                    Math/abs
                    (> config/TOO_STEEP)))
       (apply sorted-set-by nearly-same-dist)))

(defn result-max-abs-slope [segment]
  (->> (extract-from-segment segment :slope)
       (map #(Math/abs %)) ; wrapping in #(), see https://stackoverflow.com/questions/35199808/clojure-unable-to-find-static-field
       (reduce max)))

(defn write-result [segment]
  (-> segment
      (assoc-in [:results :max-abs-slope] (result-max-abs-slope segment))))

(defn write-issues [segment]
  (->> (issue-too-steep-points segment)
       (map #(hash-map :msg "Slope to high/low"
                       :point %))
       (apply update segment :issues conj)))

(defn filter-seqs [filter-width data]
  (->> filter-width
       (range)
       (map #(->> data
                  (split-at %)
                  second))))

(defn median [& args]
  (let [sorted (sort args)
        cnt (count sorted)
        halfway (quot cnt 2)]
    (if (odd? cnt)
      (nth sorted halfway) ; (1)
      (let [bottom (dec halfway)
            bottom-val (nth sorted bottom)
            top-val (nth sorted halfway)]
        (/ (+ bottom-val top-val) 2)))))
