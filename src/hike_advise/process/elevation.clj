(ns hike-advise.process.elevation)

(defn calc [segment]
  (-> segment
      :data
      (->> (map :elevation))))

(defn total-asc [segment]
  (let [elevation (calc segment)]
    (->> elevation
         (map - (rest elevation))
         (cons 0)
         (map #(max % 0))
         (reduce +))))

(defn total-desc [segment]
  (let [elevation (calc segment)]
    (->> elevation
         (map - (rest elevation))
         (cons 0)
         (map #(min % 0))
         (reduce +)
         Math/abs)))
