(ns hike-advise.process.distance
  (:require [hike-advise.convert :as convert]
            [hike-advise.config :as config]))

(declare distance-km point-to-radians to-radians calc-dist)

(def radius-earth 6371) ; [km]

(defn total-km [segment]
  (->> (calc-dist segment) (reduce +)))

(defn calc-dist [segment]
  (->> (:data segment)
       (#(map distance-km % (rest %)))
       (cons 0)))

(defn calc-cumul-dist [segment]
  (->> (convert/extract-from-segment segment :distance)
      seq
      (reduce #(conj % (+ (first %) %2)) (seq [0]))
      reverse
      rest))

(defn write-dist [segment]
  (->> (calc-dist segment)
       (convert/write-to-segment segment :distance)))

(defn write-cumul-dist [segment]
  (->> (calc-cumul-dist segment)
       (convert/write-to-segment segment :cumul-distance)))

(defn write-calc [segment]
  (-> segment write-dist write-cumul-dist))

(defn write-result [segment]
     (-> segment
         (assoc-in [:results :total-distance] (total-km segment))))

(defn write-issues [segment]
  (->> (:data segment)
       (filter #(> (:distance %) config/NEAR_DISTANCE))
       (map #(hash-map :msg "Point to far from previous point"
                       :point %))
       (apply update segment :issues conj)))

(defn rescale [g-earth-total-dist segment]
  (let [total-dist (total-km segment)
        conversion-factor (/ g-earth-total-dist total-dist)]
    (-> segment
        (convert/extract-from-segment :cumul-distance)
        (->> (map (partial * conversion-factor)))
        (->> (convert/write-to-segment segment :cumul-distance)))))

(defn distance-km [p1 p2]
  "Calculated with haversine formula."
  (let [{lat1 :lat lon1 :lon} (point-to-radians p1)
        {lat2 :lat lon2 :lon} (point-to-radians p2)
        dlon (- lon2 lon1)
        dlat (- lat2 lat1)]
    (-> (+ (-> dlat (/ 2) Math/sin (Math/pow 2))
           (* (-> dlon (/ 2) Math/sin (Math/pow 2))
              (Math/cos lat1)
              (Math/cos lat2)))
        Math/sqrt
        Math/asin
        (* 2 radius-earth))))

(defn point-to-radians [point]
  (zipmap
   (keys point)
   (map to-radians (vals point))))

(defn to-radians [angle]
  (-> angle
      (/ 180)
      (* Math/PI)))
