(ns hike-advise.read.gpx
  (:require [clojure.data.xml]))

(declare parse-xml first-element point-to-map point-to-elevation)

(defn file-to-segment [file-obj]
  {:file file-obj
   :data (parse-xml file-obj)
   :type :hike
   :issues []})

; also works with filepath
(defn parse-xml [file-obj]
  (-> file-obj
      clojure.java.io/input-stream
      clojure.data.xml/parse
      :content
      (first-element :trk)
      (first-element :trkseg)
      (->> (map point-to-map))))

(defn first-element [xml-tree element-key]
  (->> xml-tree
       (filter #(= (get % :tag) element-key))
       first
       :content))

(defn point-to-map [{{:keys [:lon :lat]} :attrs :as xml-point}]
  (merge {:lon (Double/parseDouble lon)
          :lat (Double/parseDouble lat)}
         (point-to-elevation xml-point)))

(defn point-to-elevation [xml-point]
  (-> xml-point
      :content
      (first-element :ele)
      first
      Double/parseDouble
      (->> (hash-map :elevation))))
