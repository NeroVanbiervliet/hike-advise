(ns hike-advise.read.general
  "Read files and process their contents."
  (:require [clojure.string :as string]
            [hike-advise.read.gpx :as gpx]))

(declare get-extension processable? file-to-segment)

(def readers
  {"gpx" gpx/file-to-segment})

(defn read-files [path]
  (->
   (clojure.java.io/file path)
   (file-seq)
   (->> (filter processable?))
   (->> (map file-to-segment))))

(defn get-extension [file]
  (-> file
      str
      (string/split #"\.")
      last))

(defn processable? [file]
  (contains? readers (get-extension file)))

(defn file-to-segment [file]
  ((get readers (get-extension file)) file))
