(ns hike-advise.core
  "Read, process and plot the hike planning files."
  (:require [clojure.pprint :refer [pprint]]
            [hike-advise.read.general :as read]
            [hike-advise.plot.elevation :as elevation]
            [hike-advise.process.load :as process.load]
            [hike-advise.process.distance :as distance]
            [hike-advise.process.slope :as slope]
            [hike-advise.output.report :as report])
  (:gen-class))

(declare analyse-load)

(def gps-files
  (-> "./data"
      read/read-files))

(defn print-result [segment]
  (println "RESULTS")
  (->> segment :results pprint)
  (println "ISSUES")
  (->> segment :issues pprint))

(defn -main
  [& args]
  ; args 0 = filepath, 1 = total km in Google earth
  (let [filepath (first args)
        g-earth-total-km (-> args second Float/parseFloat)]
    (->> filepath
         read/read-files
         first
         distance/write-calc
         (distance/rescale g-earth-total-km)
         distance/write-result
         distance/write-issues
         slope/write-calc
         slope/write-result
         slope/write-issues
         process.load/write-result
         print-result)
    ))

(defn analyse-load []
  (->> gps-files
       (map process.load/calc-hike-load)
       (map println)))

(def raw-results
  (->> gps-files
       (map
        #(-> %
             distance/write-calc
             distance/write-result
             distance/write-issues
             slope/write-calc
             slope/write-result
             slope/write-issues
             process.load/write-result))))

(defn report []
  (->> raw-results
       report/format-results
       (apply println)))
