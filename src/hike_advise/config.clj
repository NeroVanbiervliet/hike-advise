(ns hike-advise.config)

; report output
(def WIDTH_COLUMN 16)

; too steep slope to climb (up OR down) [%]
(def TOO_STEEP 55)

; maximum distance between two points [km]
(def NEAR_DISTANCE 0.25)

; number of samples (points) for median filtering
(def MEDIAN_SAMPLES 3)
