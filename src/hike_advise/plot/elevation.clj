(ns hike-advise.plot.elevation
  (:require [incanter.core :refer [view]]
            [incanter.charts :refer [xy-plot]]
            [hike-advise.process.elevation :as process.elevation]
            [hike-advise.convert :as convert])
  (:gen-class))

(defn render [segment]
  (let [elevation (process.elevation/calc segment)
        distance (convert/extract-from-segment segment :cumul-distance)]
    (view (xy-plot distance elevation))))

