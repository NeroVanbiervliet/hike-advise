(ns hike-advise.core-test
  (:require [clojure.test :refer :all]
            [hike-advise.read.general :refer [file-to-segment]]
            [hike-advise.process.load :as process.load]
            [hike-advise.process.distance :as distance]
            [hike-advise.process.slope :as slope]))

(def test-segment
  (file-to-segment "./test/hike_advise/read/test.gpx"))

(def test-segment-processed
  (-> test-segment
      distance/write-calc
      slope/write-calc
      slope/write-result
      process.load/write-result))
