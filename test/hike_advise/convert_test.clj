(ns hike-advise.convert-test
  (:require [clojure.test :refer :all]
            [hike-advise.core-test :refer [test-segment]]
            [hike-advise.convert :refer :all]))

(deftest test-extract-from-segment
  (is (= [42.2 43.2 44.2]
         (extract-from-segment test-segment :lon))))

(deftest extract-after-write
  (let [test-data [:t :e :s]]
    (is (= test-data
           (-> test-segment
               (write-to-segment :test test-data)
               (extract-from-segment :test))))))
