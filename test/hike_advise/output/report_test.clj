(ns hike-advise.output.report-test
  (:require [clojure.test :refer :all]
            [hike-advise.core-test :refer [test-segment]]
            [hike-advise.output.report :refer :all]
            [hike-advise.process.distance :as distance]
            [hike-advise.process.slope :as slope]
            [hike-advise.process.load :as pload]
            [hike-advise.config :as config]))

(deftest test-format-segment-results
  (is (= '("\tmax-slope     0.007242746148407731\n"
           "\thike-load     1030.6893272073391\n")
         (-> test-segment
             distance/write-calc
             slope/write-calc
             slope/write-result
             pload/write-result
             format-segment-results))))


(deftest test-generate-spaces
  (is (= (- config/WIDTH_COLUMN 4)
         (count (generate-spaces :four)))))
