(ns hike-advise.process.slope-test
  (:require [clojure.test :refer :all]
            [hike-advise.core-test :refer [test-segment]]
            [hike-advise.process.slope :as slope]
            [hike-advise.process.distance :as distance]))


(deftest test-slope
  (is (= '(0 0.007242746148407731 -0.0145680010986492)
         (-> test-segment
             distance/write-calc
             slope/calc))))

(deftest test-write-result
  (let [results (-> test-segment
                   distance/write-calc
                   slope/write-calc
                   slope/write-result
                   :results)]
    (is (= 0.0145680010986492
           (:max-abs-slope results)))
    (is (= #{} ; empty
           (:too-steep-slopes results)))))

(deftest test-filter-seqs
  (let [d (range 15)]
    (is (= [d (rest d) (rest (rest d))]
           (slope/filter-seqs 3 d)))))

