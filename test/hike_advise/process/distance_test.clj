(ns hike-advise.process.distance-test
  (:require [clojure.test :refer :all]
            [hike-advise.core-test :refer [test-segment]]
            [hike-advise.process.distance :refer :all]
            [same :refer [ish?]]))

(deftest test-distance-km
  (let [bruges {:lat 51.2238738 :lon 3.2331298}
        ghent {:lat 51.054340 :lon 3.717424}
        distance 38.69085537341232]
    (is (ish? distance
              (distance-km bruges ghent)))))

(deftest test-total-km
  (-> (total-km test-segment)
      (- 275.36)
      Math/abs
      (< 0.2)
      is))

(deftest test-distance-segment
  (is (ish? '(0 138.06917700958542 137.28719447896304)
            (calc test-segment))))

(deftest test-distance-segment2
  (let [distances
        (calc test-segment)]
    (is (= 3 (count distances)))
    (is (ish? (total-km test-segment) (reduce + distances)))))
