(ns hike-advise.process.elevation-test
    (:require [clojure.test :refer :all]
              [hike-advise.core-test :refer [test-segment]]
              [hike-advise.process.elevation :refer :all]
              [same :refer [ish?]]))

(deftest test-asc-desc
  (is (ish? 1.0 (total-asc test-segment)))
  (is (ish? 2.0 (total-desc test-segment))))

