(ns hike-advise.read.gpx-test
  (:require [clojure.test :refer :all]
            [hike-advise.read.gpx :refer :all]))

(deftest parse-xml-file
  (is (= '({:lon 42.2, :lat 42.1, :elevation 42.3}
           {:lon 43.2, :lat 43.1, :elevation 43.3}
           {:lon 44.2, :lat 44.1, :elevation 41.3})
         (parse-xml "./test/hike_advise/read/test.gpx"))))

