(ns hike-advise.read.general-test
  (:require [clojure.test :refer :all]
            [hike-advise.read.general :refer :all]))


(deftest file-extensions
  (is (= "kml"
         (get-extension "some-file.kml"))))
