# hike-advise

*An advisor for hiking trips planned in Google Earth*



### Usage

1. Draw your path in Google Earth
2. Draw the elevation profile and note down the total distance of the track (TD)
3. Save place as `.kmz`
4. Convert to gpx using [GPS Visualizer website](https://www.gpsvisualizer.com/elevation)
5. Put the file in `data` directory
6. `java -jar target/uberjar/*-standalone.jar data/<your-file>.gpx <g. earth TD>`